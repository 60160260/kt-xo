package org.kotlinlang.play

var table = arrayOf(charArrayOf('-','-','-'),
        charArrayOf('-','-','-'),
        charArrayOf('-','-','-'))

fun main() {
    var count = 0
    var turn = ' '
    var end = ' '

    while (end == ' ') {
        while (true) {
            try {
                print("Please input row,col: ")
                val input = readLine()
                val numStr = input?.split(" ")

                try {
                    if (numStr?.size ?: 0 == 2) {

                        val row = numStr?.get(0)?.toInt()
                        val col = numStr?.get(1)?.toInt()
//                        println("$row,$col")

                        if (table[row!!-1][col!!-1] == '-'){
                            if (count%2 == 0){
                                table[row!! - 1][col!! - 1] = 'X'
                                count+=1
                                turn = 'X'
                            }else{
                                table[row!! - 1][col!! - 1] = 'O'
                                count+=1
                                turn = 'O'
                            }
                        }else{
                            println("Please input new row,col")
                        }

                        end = checkWin(turn, end)

                        break
                    } else {
                        println("Please input row,col in NumberFormat")
                    }
                }catch (e:ArrayIndexOutOfBoundsException){
                    println("Please input number 1 - 3")
                }

            } catch (e: NumberFormatException) {
                println("Please input row,col in NumberFormat")
            }
        }


        for (row in table) {
            for (col in row) {
                print(col)
            }
            println()
        }

        if (end == 'e'){
            print("NEW GAME (Y/N) ")
            val newgame = readLine()
            if (newgame == "Y"){
                table = arrayOf(charArrayOf('-','-','-'),
                    charArrayOf('-','-','-'),
                    charArrayOf('-','-','-'))
                end = ' '
            }else{
                println("THANK YOU")
            }

        }
    }

}

private fun checkWin(turn: Char, end: Char): Char {
    var end1 = end
    if (table[0][0] == turn && table[0][1] == turn && table[0][2] == turn) {
        println("$turn Win")
        end1 = 'e'
    } else if (table[1][0] == turn && table[1][1] == turn && table[1][2] == turn) {
        println("$turn Win")
        end1 = 'e'
    } else if (table[2][0] == turn && table[2][1] == turn && table[2][2] == turn) {
        println("$turn Win")
        end1 = 'e'
    } else if (table[0][0] == turn && table[1][0] == turn && table[2][0] == turn) {
        println("$turn Win")
        end1 = 'e'
    } else if (table[0][1] == turn && table[1][1] == turn && table[2][1] == turn) {
        println("$turn Win")
        end1 = 'e'
    } else if (table[0][2] == turn && table[1][2] == turn && table[2][2] == turn) {
        println("$turn Win")
        end1 = 'e'
    } else if (table[0][0] == turn && table[1][1] == turn && table[2][2] == turn) {
        println("$turn Win")
        end1 = 'e'
    } else if (table[2][0] == turn && table[1][1] == turn && table[0][2] == turn) {
        println("$turn Win")
        end1 = 'e'
    }
    return end1
}